package org.apache.lucene.solr.analyzer;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class PinYinTests {


    public static void main(String[] args) throws BadHanyuPinyinOutputFormatCombination {
        // 设置输出格式
        HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
        outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
        // 保持其他字符
        System.out.println(PinyinHelper.toHanYuPinyinString("重庆123aBc重量", outputFormat, "", true));
        // 忽略其他字符
        System.out.println(PinyinHelper.toHanYuPinyinString("重庆123aBc重量", outputFormat, "", false));
    }

}
