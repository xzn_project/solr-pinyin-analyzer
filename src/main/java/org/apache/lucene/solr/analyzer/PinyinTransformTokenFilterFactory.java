package org.apache.lucene.solr.analyzer;

import java.util.Map;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.TokenFilterFactory;

/**
 * @author XUZUONIAN
 */
public class PinyinTransformTokenFilterFactory extends TokenFilterFactory {
    private boolean isFirstChar = false;
    private boolean isOutChinese = true;
    private int minTermLenght = 2;

    public PinyinTransformTokenFilterFactory(Map<String, String> args) {
        super(args);
        this.isFirstChar = getBoolean(args, "isFirstChar", false);
        this.isOutChinese = getBoolean(args, "isOutChinese", true);
        this.minTermLenght = getInt(args, "minTermLenght", 2);
    }

    @Override
    public TokenFilter create(TokenStream input) {
        return new PinyinTransformTokenFilter(input, this.isFirstChar, this.minTermLenght, this.isOutChinese);
    }
}