# solr-pinyin-analyzer

#### 项目介绍
solr 拼音分词 copy 之后的

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2018/0928/183827_88a2b0bc_336132.png "QQ截图20180928183641.png")


#### 安装教程
在solr 中 对应目录的managed-schema 添加如下配置

```
<fieldType name="text_pinyin" class="solr.TextField">
    <analyzer type="index">
      <tokenizer class="org.apache.lucene.solr.analyzer.lucene.IKTokenizerFactory"/>       
	  <filter class="org.apache.lucene.solr.analyzer.PinyinTransformTokenFilterFactory" minTermLenght="2" />
      <filter class="org.apache.lucene.solr.analyzer.PinyinNGramTokenFilterFactory" minGram="1" maxGram="20" />
    </analyzer>
    <analyzer type="query">
      <tokenizer class="org.apache.lucene.solr.analyzer.lucene.IKTokenizerFactory"/> 
	  <filter class="org.apache.lucene.solr.analyzer.PinyinTransformTokenFilterFactory" minTermLenght="2" />
      <filter class="org.apache.lucene.solr.analyzer.PinyinNGramTokenFilterFactory" minGram="1" maxGram="20" />
    </analyzer>
  </fieldType>
```

